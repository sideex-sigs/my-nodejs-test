var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/a', function(req, res, next) {
  res.render('index', { title: 'Express a' });
});

router.get('/ccccc', function(req, res, next) {
  res.render('index', { title: 'Express ccccc' });
});

router.get('/bbbbb', function(req, res, next) {
  res.render('index', { title: 'Express bbbbb' });
});

module.exports = router;
